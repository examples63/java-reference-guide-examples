# Java reference guide examples

[**Author:** Martin Kompan](https://gitlab.com/examples63/mkuseful/-/blob/main/certs/Programming_certificates.md)

Java version: **1.8**

## Environment:

Android Studio Dolphin | 2021.3.1 Patch 1

API 29: Android 10.0

## Table of contents

1. [Comments, Primitive Types,  Declaring Variables and Constants, Type Casting](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/MainActivity.java#L43)
2. [Operators](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/MainActivity.java#L103)
3. [String, Boolean, Random numbers](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/MainActivity.java#L117)
4. [If ... Else, Switch Statements](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/MainActivity.java#L155)
5. [While Loop, Do/While Loop, For loop, Break and Continue](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/MainActivity.java#L195)
6. [Arrays, Enum, Date](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/MainActivity.java#L241)
7. [ArrayList](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/MainActivity.java#L314)
8. [HashMap](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/MainActivity.java#L348)
9. [Exceptions - Try...Catch](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/MainActivity.java#L375)
10. [Lambda Expressions ( Java 8 )](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/MainActivity.java#L403)
11. [Classes, Class Access Modifiers, static methods, Properties (Setter, Getter), Instance methods](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/Main.java#L4)
12. [Inner Classes](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/Main.java#L111)
13. [Inheritance](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/Main.java#L125)
14. [Polymorphism, Abstract class](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/Main.java#L135)
15. [Interfaces](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/Main.java#L160)
16. [Threads](https://gitlab.com/examples63/java-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/java_reference_guide_examples/Main.java#L182)
