package martin.kompan.demo.java_reference_guide_examples;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.time.LocalDateTime; // Import the LocalDateTime class
import java.time.format.DateTimeFormatter; // Import the DateTimeFormatter class

import java.util.ArrayList;
import java.util.Collections;  // Import the Collections class

import java.util.HashMap; // import the HashMap class

import java.util.function.Consumer;

interface StringFunction {
    String run(String str);
}



public class MainActivity extends AppCompatActivity {

    // Lambda
    public static void printFormatted(String str, StringFunction format) {
        String result = format.run(str);
        System.out.println(result);
    }

    // Enum declaration :

    enum Level {
        LOW,
        MEDIUM,
        HIGH
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        String[] names1 = new String[] {"Ava", "Emma", "Olivia"};
        String[] names2 = new String[] {"Olivia", "Sophia", "Emma"};

        System.out.println(String.join(", ", MergeNames.uniqueNames(names1, names2)));












//
//        // Single line commet
//
//        /*
//        Multi line
//        comment
//        */
//
//
//        // Console output :
//
//        System.out.println("Hello World");
//        System.out.println(358 + 10);
//
//
//        // Primitive Types
//
////        byte
////        short
////        int
////        long
////        float
////        double
////        char
////        boolean
//
//
//        // Declaring Variables
//
//        String name = "John";
//        System.out.println(name);
//
//        byte age = 30;
//
//        long viewsCount = 3_123_456L;
//
//        float price = 10.99F;
//
//        char letter = 'A';
//
//        boolean isEligible = true;
//
//
//        // Declare constant
//
//        final int x;
//        x = 7;
//
//
//        // Type Casting
//
//        int myInt = 9;
//        double myDouble = myInt; // Automatic casting: int to double
//
//        System.out.println(myInt);      // Outputs 9
//        System.out.println(myDouble);   // Outputs 9.0
//
//        double myDouble2 = 9.78d;
//        myInt = (int) myDouble; // Manual casting: double to int
//
//
//        // Operators
//
//        // Arithmetic Operators: + - * / % ++ --
//
//        myInt++;
//        ++myInt;
//
//        // Assignment Operators:  = += -= *= /= %= &= |= ^= >>= <<=
//
//        // Comparison Operators: == != < > <= >=
//
//        // Logical Operators:   &&  ||  !
//
//
//        // String
//
//        String txt = "hello world";
//        System.out.println("The length of the txt string is: " + txt.length());
//
//        System.out.println(txt.toUpperCase());   // Outputs "HELLO WORLD"
//        System.out.println(txt.toLowerCase());   // Outputs "hello world"
//
//        System.out.println(txt.indexOf("world"));
//
//        String firstName = "John";
//        String lastName = "Doe";
//        System.out.println(firstName + " " + lastName);
//
//        // Special escape Characters  :  \'  \"  \\
//
////          \n 	New Line
////          \r 	Carriage Return
////          \t 	Tab
////          \b 	Backspace
////          \f 	Form Feed
//
//        System.out.println("We are the so-called \"Vikings\" from the north.");
//
//
//        // Random
//
//        int randomNum = (int)(Math.random() * 101);  // 0 to 100
//
//
//        // Boolean
//
//        boolean isJavaFun = true;
//        boolean isFishTasty = false;
//        System.out.println(isJavaFun);     // Outputs true
//        System.out.println(isFishTasty);   // Outputs false
//
//
//        // If ... Else
//
//        if (20 > 18) {
//            System.out.println("20 is greater than 18");
//        }
//
//        int time = 20;
//        if (time < 18) {
//            System.out.println("Good day.");
//        } else {
//            System.out.println("Good evening.");
//        }
//
//        if (time < 10) {
//            System.out.println("Good morning.");
//        } else if (time < 18) {
//            System.out.println("Good day.");
//        } else {
//            System.out.println("Good evening.");
//        }
//
//        String result = (time < 18) ? "Good day." : "Good evening.";
//        System.out.println(result);
//
//
//        // Switch Statements
//
//        int day = 4;
//        switch (day) {
//            case 6:
//                System.out.println("Today is Saturday");
//                break;
//            case 7:
//                System.out.println("Today is Sunday");
//                break;
//            default:
//                System.out.println("Looking forward to the Weekend");
//        }
//
//
//        // While Loop
//
//        int i = 0;
//        while (i < 5) {
//            System.out.println(i);
//            i++;
//        }
//
//
//        // Do/While Loop
//
//        do {
//            System.out.println(i);
//            i++;
//        }
//        while (i < 10);
//
//
//        // For loop
//
//        for (int b = 0; b < 5; b++) {
//            System.out.println(b);
//        }
//
//        String[] cars = {"Volvo", "BMW", "Ford", "Mazda"};
//        for (String car : cars) {
//            System.out.println(car);
//        }
//
//        // Break and Continue
//
//        for (int m = 0; m < 10; m++) {
//            if (m == 4) {
//                break;
//            }
//            System.out.println(m);
//        }
//
//        for (int n = 0; n < 10; n++) {
//            if (n == 4) {
//                continue;
//            }
//            System.out.println(n);
//        }
//
//
//        // Arrays
//
//        int[] myNum = {10, 20, 30, 40};
//
//        // String[] cars = {"Volvo", "BMW", "Ford", "Mazda"};
//
//        System.out.println(cars[0]);
//
//        cars[0] = "Opel";
//
//        System.out.println(cars.length);
//
//        for (int t = 0; t < cars.length; t++) {
//            System.out.println(cars[t]);
//        }
//
//        for (String c : cars) {
//            System.out.println(c);
//        }
//
//        // Multidimensional
//
//        int[][] myNumbers = { {1, 2, 3, 4}, {5, 6, 7} };
//        myNumbers[1][2] = 9;
//        System.out.println(myNumbers[1][2]); // Outputs 9 instead of 7
//
//
//        // Enums
//
////        enum Level {
////            LOW,
////            MEDIUM,
////            HIGH
////        }
//
//        Level myVar = Level.MEDIUM;
//
//        switch(myVar) {
//            case LOW:
//                System.out.println("Low level");
//                break;
//            case MEDIUM:
//                System.out.println("Medium level");
//                break;
//            case HIGH:
//                System.out.println("High level");
//                break;
//        }
//
//        for (Level all : Level.values()) {
//            System.out.println(all);
//        }
//
//        //  Date
//
////        import java.time.LocalDateTime; // Import the LocalDateTime class
////        import java.time.format.DateTimeFormatter; // Import the DateTimeFormatter class
//
//        LocalDateTime myDateObj = LocalDateTime.now();
//        System.out.println("Before formatting: " + myDateObj);
//        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
//
//        String formattedDate = myDateObj.format(myFormatObj);
//        System.out.println("After formatting: " + formattedDate);
//
//        // ofPattern() :
//
////        yyyy-MM-dd 	    "1988-09-29"
////        dd/MM/yyyy 	    "29/09/1988"
////        dd-MMM-yyyy 	    "29-Sep-1988"
////        E, MMM dd yyyy 	"Thu, Sep 29 1988"
//
//
//        // ArrayList
//
//        // import java.util.ArrayList;
//
//        ArrayList<String> cars2 = new ArrayList<String>();
//        cars2.add("Volvo");
//        cars2.add("BMW");
//        cars2.add("Ford");
//        cars2.add("Mazda");
//        System.out.println(cars2);
//
//        System.out.println(cars2.get(0));
//
//        cars2.set(0, "Opel");
//
//        cars2.remove(0);
//
//        System.out.println(cars2.size());
//
//        for (int o = 0; o < cars2.size(); o++) {
//            System.out.println(cars2.get(o));
//        }
//
//        // import java.util.Collections;  // Import the Collections class
//
//        Collections.sort(cars2);  // Sort cars
//
//        for (String u : cars2) {
//            System.out.println(u);
//        }
//
//        cars2.clear();
//
//
//        // HashMap
//
//        // import java.util.HashMap; // import the HashMap class
//
//        HashMap<String, String> capitalCities = new HashMap<String, String>();
//
//        capitalCities.put("England", "London");
//        capitalCities.put("Germany", "Berlin");
//        capitalCities.put("Norway", "Oslo");
//        capitalCities.put("USA", "Washington DC");
//
//        System.out.println(capitalCities);
//
//        System.out.println(capitalCities.get("England"));
//
//        capitalCities.remove("England");
//
//        System.out.println(capitalCities.size());
//
//        // Print keys and values
//        for (String r : capitalCities.keySet()) {
//            System.out.println("key: " + r + " value: " + capitalCities.get(r));
//        }
//
//        capitalCities.clear();
//
//
//        // Exceptions - Try...Catch
//
//        try {
//            int[] myNumbers2 = {1, 2, 3};
//            System.out.println(myNumbers2[10]);
//        } catch (Exception e) {
//            System.out.println("Something went wrong.");
//        }
//
//        try {
//            int[] myNumbers3 = {1, 2, 3};
//            System.out.println(myNumbers3[10]);
//        } catch (Exception e) {
//            System.out.println("Something went wrong.");
//        } finally {
//            System.out.println("The 'try catch' is finished.");
//        }
//
//        // throw
//
//        if (age < 18) {
//            throw new ArithmeticException("Access denied - You must be at least 18 years old.");
//        }
//        else {
//            System.out.println("Access granted - You are old enough!");
//        }
//
//
//        // Lambda
//
//        // Lambda Expressions were added in Java 8.
//
//        // Syntax:
//
////      parameter -> expression
////      (parameter1, parameter2) -> expression
////      (parameter1, parameter2) -> { code block }
//
//        ArrayList<Integer> numbers = new ArrayList<Integer>();
//        numbers.add(5);
//        numbers.add(9);
//        numbers.add(8);
//        numbers.add(1);
//        numbers.forEach( (n) -> { System.out.println(n); } );
//
//        // Consumer
//
//        // import java.util.function.Consumer;
//
//        Consumer<Integer> method = (n) -> { System.out.println(n); };
//        numbers.forEach( method );
//
////        interface StringFunction {
////            String run(String str);
////        }
//
////        public static void printFormatted(String str, StringFunction format) {
////            String result = format.run(str);
////            System.out.println(result);
////        }
//
//        StringFunction exclaim = (s) -> s + "!";
//        StringFunction ask = (s) -> s + "?";
//        printFormatted("Hello", exclaim);
//        printFormatted("Hello", ask);
//
//
    }


}


