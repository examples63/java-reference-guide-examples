package martin.kompan.demo.java_reference_guide_examples;


    // Class Access Modifiers

//public 	    The class is accessible by any other class

//default 	    The class is only accessible by classes in the same package.

// Non-Access Modifiers

//final 	    The class cannot be inherited by other classes

//abstract 	    The class cannot be used to create objects (To access an abstract class, it must be inherited from another class.


public class Main {


    // A static method means that it can be accessed without creating an object of the class

    static void myMethod() {
        System.out.println("I just got executed!");
    }

    static void myMethod2(String fname) {
        System.out.println(fname + " Refsnes");
    }

    static int plusMethodInt(int x, int y) {
        return x + y;
    }

    static double plusMethodDouble(double x, double y) {
        return x + y;
    }


    // Access Modifiers : attributes, methods and constructors

//    public 	    The code is accessible for all classes
//
//    private 	    The code is only accessible within the declared class
//
//    default 	    The code is only accessible in the same package.
//
//    protected 	The code is accessible in the same package and subclasses.

    // Non-Access Modifiers : attributes and methods

//    final 	        Attributes and methods cannot be overridden/modified

//    static 	        Attributes and methods belongs to the class, rather than an object

//    abstract 	    Can only be used in an abstract class, and can only be used on methods. The method does not have a body, for example abstract void run();. The body is provided by the subclass (inherited from).

//    transient 	    Attributes and methods are skipped when serializing the object containing them

//    synchronized 	Methods can only be accessed by one thread at a time

//    volatile 	    The value of an attribute is not cached thread-locally, and is always read from the "main memory"


    // Properties

    int x = 5;
    int num;

    String name = "Main";

    // Getter
    public String getName() {
        return name;
    }

    // Setter
    public void setName(String newName) {
        this.name = newName;
    }

    // Declare constant
    final int y = 10;

    // Constructors

    public Main() {
        num = 20;  // Set the initial value for the attribute num
    }

    public Main(int num, String name) {
        this.num = num;
        this.name = name;
    }

    // Instance (object) method

    public void myPublicMethod() {
        System.out.println("Public methods must be called by creating objects");
    }

    public void speed(int maxSpeed) {
        System.out.println("Max speed is: " + maxSpeed);
    }

    public static void main(String[] args) {
        Main myObj = new Main();
        System.out.println(myObj.x);
    }


    // Inner Classes

    class InnerClass {
        public int myInnerMethod() {
            return x;
        }
    }

    private class PrivateInnerClass {
        int z = 5;
    }
}


// Inheritance

//    subclass        (child) - the class that inherits from another class
//    superclass      (parent) - the class being inherited from

class Car extends Main {
    private String modelName = "Mustang";    // Car attribute
}


// Polymorphism

// Abstract class
abstract class Animal {
    // Abstract method (does not have a body)
    public abstract void animalSound();
    // Regular method
    public void sleep() {
        System.out.println("Zzz");
    }
}

class Pig extends Animal {
    public void animalSound() {
        System.out.println("The pig says: wee wee");
    }
}

class Dog extends Animal {
    public void animalSound() {
        System.out.println("The dog says: bow wow");
    }
}


// Interfaces

// An interface is a completely "abstract class" that is used to group related methods with empty bodies.

interface FirstInterface {
    public void myMethod(); // interface method
}

interface SecondInterface {
    public void myOtherMethod(); // interface method
}

class DemoClass implements FirstInterface, SecondInterface {
    public void myMethod() {
        System.out.println("Some text..");
    }
    public void myOtherMethod() {
        System.out.println("Some other text...");
    }
}


// Threads

class ThreadsExample extends Thread {
    public static int amount = 0;

    public static void main(String[] args) {
        ThreadsExample thread = new ThreadsExample();
        thread.start();
        // Wait for the thread to finish
        while(thread.isAlive()) {
            System.out.println("Waiting...");
        }
        // Update amount and print its value
        System.out.println("ThreadsExample: " + amount);
        amount++;
        System.out.println("ThreadsExample: " + amount);
    }

    public void run() {
        amount++;
    }
}
