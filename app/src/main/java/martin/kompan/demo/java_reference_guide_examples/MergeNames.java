package martin.kompan.demo.java_reference_guide_examples;

public class MergeNames {

    public static String[] uniqueNames(String[] names1, String[] names2) {

        String[] both = new String[0];

        for (int i=0;  i < names1.length ;i = i+1 ) {
            if (!contains(both, names1[i])) {
                String[] newArr = new String[both.length +1];
                for (int n=0; n< both.length; n++) {
                    newArr[n] = both[n];
                }
                newArr[both.length] = names2[i];
            }
        }

        for (int j=0;  j < names2.length ;j = j+1 ) {
            if (!contains(both, names2[j])) {
                String[] newArr = new String[both.length +1];
                for (int m=0; m< both.length; m++){
                    newArr[m]=both[m];
                }
                newArr[both.length] = names2[j];
            }
        }

        return both;
    }

    public static boolean contains(String[] names, String name) {

        for (int i=0;  i < names.length ;i = i+1 ) {
            if (names[i] == name) { return true ;}
        }

        return false;
    }

    public static void main(String[] args) {
        String[] names1 = new String[] {"Ava", "Emma", "Olivia"};
        String[] names2 = new String[] {"Olivia", "Sophia", "Emma"};
        System.out.println(String.join(", ", MergeNames.uniqueNames(names1, names2))); // should print Ava, Emma, Olivia, Sophia
    }
}
