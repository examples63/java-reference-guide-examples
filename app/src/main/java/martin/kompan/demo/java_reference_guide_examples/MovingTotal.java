package martin.kompan.demo.java_reference_guide_examples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MovingTotal {

    int[] internal = new int[5];
    int[] lastTwo = {0,0};

    /**
     * Adds/appends list of integers at the end of internal list.
     */
    public void append(int[] list) {

        for (int i = 0; i < list.length;  i = i+1) {
            int newnum = lastTwo[0] + lastTwo[1] + list[i];
            if ( contains(newnum) ) {
                lastTwo[0] = lastTwo[1];
                lastTwo[1] = list[i];
            } else {
                lastTwo[0] = lastTwo[1];
                lastTwo[1] = list[i];
                int newarr[] = new int[internal.length + 1];
                for (i = 0; i < internal.length; i++)
                    newarr[i] = internal[i];

                newarr[internal.length] = newnum;
            }
        }




//        throw new UnsupportedOperationException("Waiting to be implemented.");
    }

    /**
     * Returns boolean representing if any three consecutive integers in the
     * internal list have given total.
     */
    public boolean contains(int total) {
//        throw new UnsupportedOperationException("Waiting to be implemented.");
//        List<Integer> myIntList = new ArrayList<>(Arrays.asList(internal));
        return Arrays.stream(internal).anyMatch(i -> i == total);
    }

    public static void main(String[] args) {
        MovingTotal movingTotal = new MovingTotal();

        movingTotal.append(new int[] { 1, 2, 3, 4 });

        System.out.println(movingTotal.contains(6));
        System.out.println(movingTotal.contains(9));
        System.out.println(movingTotal.contains(12));
        System.out.println(movingTotal.contains(7));

        movingTotal.append(new int[] { 5 });

        System.out.println(movingTotal.contains(6));
        System.out.println(movingTotal.contains(9));
        System.out.println(movingTotal.contains(12));
        System.out.println(movingTotal.contains(7));
    }
}
