package martin.kompan.demo.java_reference_guide_examples;

public class DriverExam {
    public static void executeExercise(IExercise exercise) {

//        Exercise wright = (Exercise) exercise;

        try {
            exercise.start();
        } catch (Exception ex) {
            exercise.markNegativePoints();
        } finally {

            try {
                exercise.execute();
            } catch (Exception ex) {
                exercise.markNegativePoints();
            }

            exercise.end();
        }
//                throw new UnsupportedOperationException("Waiting to be implemented.");


    }

    public static void main(String[] args) {
        DriverExam.executeExercise(new Exercise());
    }
}

class Exercise implements IExercise {
    public void start() { System.out.println("Start"); }
    public void execute() { System.out.println("Execute"); }
    public void markNegativePoints() { System.out.println("MarkNegativePoints"); }
    public void end() { System.out.println("End"); }
}

interface IExercise {
    void start() throws Exception;
    void execute();
    void markNegativePoints();
    void end();
}
